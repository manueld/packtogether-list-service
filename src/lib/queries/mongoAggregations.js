const aggFindItems = userId => {
    return [
        {
            '$match': {
                'userId': userId
            }
        },
        {
            '$unset': [
                '_id',
            ]
        }
    ]
};

const aggFindItemInList = (userId, listId, itemId) => {
    return [
        {
            '$match': {
                'userId': userId,
                'listId': listId
            }
        }, {
            '$unwind': {
                'path': '$groups'
            }
        }, {
            '$unwind': {
                'path': '$groups.items'
            }
        }, {
            '$match': {
                'groups.items.itemId': itemId
            }
        }, {
            '$addFields': {
                'item': '$groups.items'
            }
        }, {
            '$unset': [
                '_id',
                'groups'
            ]
        }
    ]
};

module.exports = {
    aggFindItems,
    aggFindItemInList
};
