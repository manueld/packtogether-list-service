const parseCountFromTitle = (title) => {
    const regex = /^(\d{1,})([0-9a-zA-z _\.\-:()\[\]]*)$/;
    const match = String(title).match(regex);
    if (match && match.length >= 3) {
        const count = Number(match[1]);
        const parsedTitle = count === 1 ? String(title).trim() : match[2].trim(); // Keep "1" prefix when entered "1 Jeans" - the "1" is not shown in UI
        return {
            count,
            title: parsedTitle
        };
    }
    return {
        count: 1,
        title: String(title).trim()
    };
};

module.exports = {
    parseCountFromTitle
};
