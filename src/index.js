const cote = require('cote');
const MongoClient = require('mongodb');
const qs = require('querystring');

const listResponderService = new cote.Responder({
    name: 'List Service',
    key: 'list'
});

const getMongoUrl = () => {
    const username = process.env.MONGODB_USER || '';
    const pass = process.env.MONGODB_PASSWORD || '';
    const host = process.env.MONGODB_HOST || 'localhost:27017';
    let cred = '';
    if (username && pass) {
        cred = `${qs.escape(username)}:${qs.escape(pass)}@`;
    }
    const uri = `${cred}${host}/data?readPreference=primary&ssl=false&authSource=data`;
    return `mongodb://${uri}`;
}

const createDbConnection = () => {
    return MongoClient.connect(getMongoUrl(), {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            loggerLevel: 'info'
        }
    );
};
const init = async () => {
    const mongoClient = await createDbConnection();

    const listsService = new (require('./services/ListsService'))(listResponderService, mongoClient);

    // Lists
    listResponderService.on('getAllListsByUserId', req =>listsService.getAllLists(req.query));
    listResponderService.on('getListContent', req =>listsService.getListContent(req.query));
    listResponderService.on('addList', req =>listsService.addList(req.query));

    // ListGroup
    listResponderService.on('addListGroup', req =>listsService.addGroup(req.query));

    // ListItem
    listResponderService.on('addListItem', req =>listsService.addItem(req.query));
    listResponderService.on('setListitemCountPacked', req =>listsService.setItemCountPacked(req.query));

    process.on('SIGABRT', () => mongoClient.close());
    process.on('SIGBREAK', () => mongoClient.close());
}

init();
