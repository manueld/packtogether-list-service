const mongoAggregations = require('../lib/queries/mongoAggregations');
const uuid = require('uuid').v4;
const { parseCountFromTitle } = require('../lib/parser');

class ListsService {
    constructor(listResponderService, mongoClient) {
        this.responder = listResponderService;
        this.database = mongoClient.db('data');
        this.dataCollection = this.database.collection('lists');
    }

    async getAllLists({ userId }) {
        let data = null;
        try {
            const agg = mongoAggregations.aggFindItems(userId);
            data = await this.dataCollection.aggregate(agg).toArray();
        } catch(err) {
            console.error(err);
        }
        return { data };
    }

    async addList({ userId, listTitle }) {
        try {
            const newList = {
                userId,
                listId: uuid(),
                title: listTitle.trim(),
                groups: []
            };
            const { result } = await this.dataCollection.insertOne(newList);
            return result.ok ? {
                data: {
                    lists: [newList],
                    newListId: newList.listId
                }
            } :  {
                errCode: '201001',
                errKey: 'Unknown...'
             }
        } catch(err) {
            console.error(err);
            return {
                errCode: 500,
                errKey: err.message
            };
        }
    }

    async addGroup({ userId, listId, groupTitle }) {
        try {
            const newGroup = {
                groupId: uuid(),
                title: groupTitle,
                items: []
            };
            const filterQuery = {
                'userId': userId,
                'listId': listId
            };
            const updateQuery = {
                '$addToSet': {
                    'groups': newGroup
                }
            };
            const { result } = await this.dataCollection.updateOne(filterQuery, updateQuery);
            return result.ok ? {
                data: newGroup
             } : {
                errCode: 'XYZ',
                errKey: 'Unknown...'
             };
        } catch(err) {
            console.error(err);
            return {
                errCode: 500,
                errKey: err.message
            };
        }
    }

    async removeGroup({ userId, listId, groupId }) {
    }

    async addItem({ userId, listId, groupId, fullTitle }) {
        try {
            const parsedInfo = parseCountFromTitle(fullTitle);
            const newItem = {
                itemId: uuid(),
                title: parsedInfo.title,
                count: parsedInfo.count,
                countPacked: 0
            };
            const filterQuery = {
                'userId': userId,
                'listId': listId,
                'groups.groupId': groupId
            };
            const updateQuery = {
                '$addToSet': {
                    'groups.$.items': newItem
                }
            };
            const { result } = await this.dataCollection.updateOne(filterQuery, updateQuery);
            return result.ok ? {
                data: newItem
             } : {
                errCode: 'XYZ',
                errKey: 'Unknown...'
             };
        } catch(err) {
            console.error(err);
            return {
                errCode: 500,
                errKey: err.message
            };
        }
    }

    async removeItem({ userId, listId, itemId }) {
    }

    async editItem({ userId, listId, itemId, newItemTitle }) {
    }

    async setItemCountPacked({ userId, listId, itemId, newValue }) {
        try {
            const filter = {
                userId,
                listId
            };
            const update = {
                $set: { "groups.$[].items.$[itemEntry].countPacked": Number(newValue) }
            };
            const options = {
                arrayFilters: [
                    {
                        "itemEntry.itemId": itemId
                    }
                ]
            };
            const { result } = await this.dataCollection.updateMany(filter, update, options);
            //const result = await this.database.command(command);
            const wasIncremented = result.nModified === 1;
            return result.ok ? {
                data: {
                    gotUpdate: wasIncremented
                }
            } : {
                errCode: 'XYZ',
                errKey: 'Unknown...'
            };
        } catch(err) {
            console.error(err);
            return {
                errCode: 500,
                errKey: err.message
            };
        }
    }

    async completeItem({ userId, listId, itemId }) {
    }

}

module.exports = ListsService;
