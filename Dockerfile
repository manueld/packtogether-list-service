FROM node:12.18-buster-slim

# Create app directory
RUN mkdir -p /home/node
WORKDIR /home/node

# Install app dependencies
COPY package.json package-lock.json /home/node/
RUN npm install

# Bundle app source
COPY src /home/node/src/
USER node

CMD [ "npm", "start" ]